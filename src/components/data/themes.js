export class themes { 
	constructor() {
	}

	themeDefs = [
		{ name: 'light', label: 'Light',
			primary: {text: '#000', med: '#d7ccc8', light: '#fffffb', dark: '#a69b97'},
			accent: {text: '#fff', med: '#006064', light: '#62aeb2', dark: '#00363a'},
			util: {text: '#fff', warn: '#f57c00', good: '#689f38', bad: '#955'}
		},
		{ name: 'dark', label: 'Dark',
			primary: {text: '#FFF', med: '#fffde7', light: '#000', dark: '#cccab5'},
			accent: {text: '#000', med: '#c8b7b5', light: '#444', dark: '#bbb'},
			util: {text: '#fff', warn: '#f57c00', good: '#689f38', bad: '#955'}
		},
	]

	setTheme(id) {
		if (id == null && this.active != null) { var id = this.active.id; }
		if (id == null) { id = 0; }
		this.active = this.themeDefs[id]
		this.active.id = id;
		this.genRules();
		this.setRules();
	}

	genRules() {
		this.ruleKeys = [];
		var targets = [
			{ class: 'color', style: 'color' },
			{ class: 'bg', style: 'background-color' },
			{ class: 'bor', style: 'border-color' }
		]
	// Automatic Theme Rules
		for (var i = targets.length - 1; i >= 0; i--) {
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-pri-text', rule: targets[i].style+': '+this.active.primary.text+' !important;' });
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-pri-med', rule: targets[i].style+': '+this.active.primary.med+' !important;' });
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-pri-light', rule: targets[i].style+': '+this.active.primary.light+' !important;' });
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-pri-dark', rule: targets[i].style+': '+this.active.primary.dark+' !important;' });
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-acc-text', rule: targets[i].style+': '+this.active.accent.text+' !important;' });
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-acc-med', rule: targets[i].style+': '+this.active.accent.med+' !important;' });
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-acc-light', rule: targets[i].style+': '+this.active.accent.light+' !important;' });
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-acc-dark', rule: targets[i].style+': '+this.active.accent.dark+' !important;' });
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-util-text', rule: targets[i].style+': '+this.active.util.text+' !important;' });
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-util-warn', rule: targets[i].style+': '+this.active.util.warn+' !important;' });
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-util-good', rule: targets[i].style+': '+this.active.util.good+' !important;' });
			this.ruleKeys.push({ sel: '.'+targets[i].class+'-util-bad', rule: targets[i].style+': '+this.active.util.bad+' !important;' });
		};
	// Manual push rules
		this.ruleKeys.push({ sel: 'mark', rule: 'background-color: '+this.active.accent.med+' !important;' });
		this.ruleKeys.push({ sel: 'mark', rule: 'color: '+this.active.accent.text+' !important;' });
	}	

	setRules() {
	// This function creates or replaces the CSS for the applicable theme
		var cssRuleCode = document.all ? 'rules' : 'cssRules';
		var sheetID = document.styleSheets.length - 1;
		var sheet = document.styleSheets[sheetID];
		var rules = sheet[cssRuleCode];
		for (var x = rules.length - 1; x >= 0; x--) {
		// Loop over each stylesheet rule (sr)
			var sr = rules[x];
			for (var y = this.ruleKeys.length - 1; y >= 0; y--) {
				var tr = this.ruleKeys[y];
				if (sr.selectorText == tr.sel) {
				// Update the rule
					sr.cssText = tr.rule;
				}
				else if (x == 0) {
					// Append the rule
					try { sheet.insertRule(tr.sel+' {'+tr.rule+'}', rules.length); } 
						catch(err) { try { sheet.addRule(tr.sel, tr.rule); } 
						catch(err) { console.log("Couldn't add style");} }
				}
			};
		};
	}

}