export class tools { 

	getUrlVars(href) {
		if (href == null || href == '') { var href = location.href; }
		if (/\?/g.test(href) == false ) { return {error: 'no vars in query string'}; };
		var vars = {};
		href = href.split('?')[1];
		href = href.split('&');
		for (var i = href.length - 1; i >= 0; i--) {
			var keyName = href[i].split('=')[0];
			var keyVal = href[i].split('=')[1];
			vars[keyName] = keyVal;
		};
		return vars;
	}

	eventContext(target) {
	// PURPOSE: Maps the event target's dataset, class, id, etc and returns it as an easy to parse object
	// Validation
		if (target == null) { return false; }
		var ancestor = target.closest('[data-cat]');
		if (ancestor != null && (target.dataset == null || target.dataset.cat == null)) {	target = ancestor; }
		if (target.dataset.cat == null) {return false; }
	// Define and map
		var obj = {};
		obj.cat = target.dataset.cat || '';
		// if (isNaN(obj.cat)) { obj.cat = obj.cat.toLowerCase(); }
		obj.val = target.dataset.val || '';
		// if (isNaN(obj.val)) { obj.cat = obj.cat.toLowerCase(); }
		obj.id = target.id || '';
		obj.class = target.className || '';
		obj.parent = target.parentElement;
		obj.children = target.children;
		for (var key in target.dataset) {
			obj[key] = target.dataset[key];
			// if (isNaN(obj[key])) { obj[key] = obj[key].toLowerCase(); }
		}
		return obj;
	}

	matchTest(needle, haystack) {
		if (needle == '' || haystack == '') { return false; }
		var match = new RegExp(needle.toLowerCase(), 'ig');
		if (match.test(haystack.toLowerCase())) { return true; }
		else { return false; }
	}

	reExec(needle, haystack) {
		if (needle == '' || haystack == '') { return false; }
		var match = new RegExp(needle, 'ig');
		var result = match.exec(haystack);
		return result;
	}

	remHTMLTags(str) {
		return str.replace(/(<([^>]+)>)/ig, '');
	}

	toHex(c) {
		var hex = c.toString(16);
		return hex.length == 1 ? "0" + hex : hex;
	}

	rgbToHex(r, g, b) {
		return "#" + this.componentToHex(r) + this.componentToHex(g) + this.componentToHex(b);
	}

	getRandBlue(base, offset) {
		if (offset == null) { var offset = 100; }
		var color = '#'+base+this.toHex(Math.floor(Math.random()*(255 - parseInt(offset))) + parseInt(offset));
		return color;
	}

}	