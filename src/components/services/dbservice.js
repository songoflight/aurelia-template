import { inject } from 'aurelia-framework';
import { avm } from '../data/avm';
@inject(avm)
export class dbService {
	constructor(avm) {
		this.avm = avm;
	}

/*
	CF AJAX PROXT FUNCTION MAPS
*/

	newDb(callback) {
/*
		var db = new dbCFC();
		db.setAsyncMode();
		db.setHTTPMethod("POST");
		db.setErrorHandler(this.dbError);
		db.setCallbackHandler(callback);
*/
		let db = {};
		return db;
	}

	dbError(msg) {
		console.warn('DB Service Error | '+msg);
	}

/*
	USER MANAGEMENT FUNCTIONS
*/		
	checkUser() {
		if (!this.model) { this.model = this;	}
/*
		var db = this.model.newDb(this.model.mapUser);
		db.model = this.model;
		db.getUserData();
		db.model.avm.session.fetching++;
*/		
	}

	mapUser(msg) {
		var session = this.model.avm.session;
		var router = this.model.avm.router;
		session.fetching--;
		if (msg != null && !msg.error && msg.context) {
			session.error = false;
			switch(msg.context) {
				case 'getUserData':
					if (msg.payload.userid > 0)  {
						session.loggedIn = true;
						session.user = msg.payload;
						if (router.currentInstruction.fragment == '/login') {
						// Is the user logged but currently on the login page, go to the student center
							router.navigateToRoute('student');
						}
						else if (this.model.avm.entryURL != null) {
						// Did the user arrive with a login-required page? go there now then
							this.model.avm.router.navigateToRoute(this.model.avm.entryURL);
							this.model.avm.entryURL = null;
						}
					}
					else {
						session.loggedIn = false;
						session.user = null;
						if (router.currentInstruction.config && router.currentInstruction.config.settings && router.currentInstruction.config.settings.auth === true) {
							router.navigateToRoute('welcome');
						}
					}
				break;
			}
		}
		else if (msg != null && msg.error && msg.errorMessage != '') {
			session.error = msg.errorMessage;
		}
	}

	logOut() {
		this.avm.session.user = null;
	}

}