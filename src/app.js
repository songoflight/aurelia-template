import { inject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { CssAnimator } from 'aurelia-animator-css';
import { Router } from 'aurelia-router';
import { authorizestep } from './pipelines/authorize-step';
import { dbService } from './components/services/dbservice';
import { avm } from './components/data/avm';
import { themes } from './components/data/themes';
import { tools } from './components/services/tools';
import moment from 'moment';
import fontawesome from 'fontawesome';

@inject(EventAggregator, Router, authorizestep, dbService, avm, themes, tools, CssAnimator)
export class App {
	constructor(EventAggregator, Router, authorizestep, dbService, avm, themes, tools, CssAnimator) {
		this.ea = EventAggregator;
		this.router = Router;
		this.authstep = authorizestep;
		this.avm = avm;

	// --- THIS IS **ONLY** FOR DEBUG - NO CHEATING, USE AVM{} SCOPE! ---
		window.avm = this.avm;
	// ------------------------------------------------------------------

		this.avm.db = dbService;
		this.avm.themes = themes;
		this.avm.tools = tools;
		this.avm.animator = CssAnimator;
		this.avm.router = Router;
		this.moment = moment;
		this.avm.appData = {
			title: 	"Template",
			logoImg: '/favicon.ico',
			version: 0.01
		}
/*
	Subscribe to router navigation events
*/
		this.ea.subscribe('router:navigation:processing', e => {
		// NAVIGATION IS PROCESSING ...
			if (!e.instruction.router.isExplicitNavigationBack) {
			// Refresh the user's login state
				avm.db.checkUser()
			// Close the mobile menu
				this.router.mobileMenuOpen = false;
			// Make sure the AJAX placeholder doesn't get out of bounds
				if (this.avm.session.fetching < 0) { this.avm.session.fetching = 0; }
			}
		});
		this.ea.subscribe('router:navigation:complete', e => {
		// NAVIGATION IS COMPLETED!   Yay!
			// Scroll To Top
				document.body.scrollTop = document.documentElement.scrollTop = 0;
			// Run active view-models attached() function in case we are navving within a child view-model
				if (this.avm && this.avm.active && this.avm.active.attached) { this.avm.active.attached(); }
		});
	}


	activate() {
	}

	attached() {
		this.avm.themes.setTheme();
	}

	configureRouter(config, router) {
		config.title = 'Aurelia Template';
		// config.options.pushState = true;
		config.options.root = '';
		config.addAuthorizeStep(authorizestep);
		config.mapUnknownRoutes('');
		config.map([
			{ 
				route: ['index', 'welcome', ''], 
				name: 'welcome',      
				moduleId: './pages/welcome',      
				nav: true, 
				settings: {class: "", auth: false},
				title: 'Welcome'
			},
			{ 
				route: ['template'], 
				name: 'template',      
				moduleId: './pages/template',      
				nav: true, 
				settings: {class: "", auth: false},
				title: 'Template'
			},
			{ 
				route: ['theme'], 
				name: 'theme',      
				moduleId: './pages/theme',      
				nav: true, 
				settings: {class: "", auth: false},
				title: 'Theme'
			}
		]);
		this.router = router;
		this.router.mobileMenuOpen = false;
		this.router.menuDeli = function(ev) {
			var target = ev.target;
			if (target.dataset == null || target.dataset.cat == null || target.dataset.val == null) {
				target = target.closest('[data-cat]');
			}
			if (target == null) {	return true; }
			switch (target.dataset.cat) {
				case 'link':
					var navRoute = 'welcome';
					if (target.dataset.val != '' && target.dataset.val != null) {
						navRoute = target.dataset.val;
					}
					this.avm.router.navigateToRoute(navRoute);
				break;
				case 'mobileMenu':
				if (target.dataset.val == 'toggle') {
					if (this.mobileMenuOpen == false) { this.mobileMenuOpen = true; }
					else { this.mobileMenuOpen = false;	}
				}
			}
		}
		router.avm = this.avm;
	}
}
