/*
 *
 * Make a String Possessive - i.e. David -> David's,  Jess -> Jess'
 *
*/
export class stringPossessiveValueConverter {
    toView(value) {
      if (value == null || value == '' || typeof value !== 'string') { return value; }
      if (/[^\']s$/ig.test(value)) { value += '\''; }
      else { value += '\'s'; }      
      return value;
    }
}

/**
 * Usage
 *
 * <require from="resources/value-converters/string-possessive"></require>
 * <h2> ${user.firstname | string-possessive} </h2>
 *
 */
